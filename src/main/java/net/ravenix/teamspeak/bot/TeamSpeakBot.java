package net.ravenix.teamspeak.bot;

import com.github.theholywaffle.teamspeak3.TS3Api;
import com.github.theholywaffle.teamspeak3.TS3Config;
import com.github.theholywaffle.teamspeak3.TS3Query;
import com.github.theholywaffle.teamspeak3.api.ClientProperty;
import com.github.theholywaffle.teamspeak3.api.TextMessageTargetMode;
import com.github.theholywaffle.teamspeak3.api.event.*;
import com.github.theholywaffle.teamspeak3.api.wrapper.Client;
import com.github.theholywaffle.teamspeak3.api.wrapper.ClientInfo;
import lombok.Getter;
import lombok.Setter;
import net.md_5.bungee.api.ProxyServer;
import net.ravenix.core.bungee.BungeeCore;
import net.ravenix.core.shared.ip.IIPProvider;
import net.ravenix.core.shared.name.INameStorageProvider;
import net.ravenix.core.shared.name.NameResult;
import net.ravenix.core.shared.player.ICorePlayer;
import net.ravenix.core.shared.player.ICorePlayerProvider;
import net.ravenix.teamspeak.TeamSpeakPlugin;
import net.ravenix.teamspeak.ts.TeamSpeakProvider;

import java.util.HashMap;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

public final class TeamSpeakBot {

    @Getter
    private TS3Query ts3Query;
    @Getter
    private TS3Api ts3Api;
    @Getter
    @Setter
    boolean online = false;

    public void registerBot() {
        (new Thread(() -> {
            TS3Config config = new TS3Config();
            config.setHost("91.218.64.34");
            config.setFloodRate(TS3Query.FloodRate.UNLIMITED);
            this.ts3Query = new TS3Query(config);
            this.ts3Query.connect();
            this.ts3Api = this.ts3Query.getApi();
            this.ts3Api.login("query", "6Vwu7GcB");
            this.ts3Api.selectVirtualServerByPort(9023);
            this.ts3Api.setNickname("Ravenix x System");
            this.ts3Api.setNickname("Ravenix x Verify");
            this.ts3Api.registerAllEvents();
            setOnline(true);
            addListener();
        })).start();
    }

    public void restart() {
        if (isOnline())
            this.unregister();

        ProxyServer.getInstance().getScheduler().schedule(TeamSpeakPlugin.getInstance(), this::registerBot, 5, TimeUnit.SECONDS);
    }

    public void unregister() {
        try {
            this.ts3Api.unregisterAllEvents();
            this.ts3Query.exit();
            this.ts3Api.logout();
            setOnline(false);
        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }

    public void addListener() {
        TeamSpeakProvider teamSpeakProvider = TeamSpeakPlugin.getInstance().getTeamSpeakProvider();
        ICorePlayerProvider corePlayerProvider = BungeeCore.getInstance().getCorePlayerProvider();
        INameStorageProvider nameStorageProvider = BungeeCore.getInstance().getNameStorageProvider();
        IIPProvider ipProvider = BungeeCore.getInstance().getIpProvider();
        this.ts3Api.addTS3Listeners(new TS3Listener() {
            public void onTextMessage(TextMessageEvent event) {
                (new Thread(() -> {
                    if (event.getTargetMode() != TextMessageTargetMode.CLIENT)
                        return;
                    int clientID = event.getInvokerId();
                    String message = event.getMessage();
                    if (message.toLowerCase().startsWith("!link ")) {
                        String[] args = message.toLowerCase().split("!link ");
                        if (args.length == 2) {
                            String name = args[1].toLowerCase();
                            NameResult nameResultByName = nameStorageProvider.getNameResultByName(name);
                            if (nameResultByName == null) {
                                getTs3Api().sendPrivateMessage(clientID, "Du musst auf dem Minecraft Server Online sein!");
                                getTs3Api().sendPrivateMessage(clientID, "IP: ravenix.net");
                                return;
                            }
                            ICorePlayer corePlayer = corePlayerProvider.getCorePlayer(nameResultByName.getUuid());
                            if (corePlayer == null) {
                                getTs3Api().sendPrivateMessage(clientID, "Du musst auf dem Minecraft Server Online sein!");
                                getTs3Api().sendPrivateMessage(clientID, "IP: ravenix.net");
                                return;
                            }
                            ClientInfo clientInfo = getTs3Api().getClientInfo(clientID);
                            String ipAddress = ipProvider.getIPAddress(nameResultByName.getUuid());
                            if (!ipAddress.equals(clientInfo.getIp())) {
                                getTs3Api().sendPrivateMessage(clientID, "Du hast keine Berechtigung um dich mit diesem Account zu verifizieren!");
                                return;
                            }
                            String tsuid = teamSpeakProvider.getTSUID(nameResultByName.getUuid());
                            if (tsuid != null) {
                                if (!tsuid.equals("noid")) {
                                    if (!tsuid.equals(clientInfo.getUniqueIdentifier())) {
                                        getTs3Api().sendPrivateMessage(clientID, "Dieser Minecraft Account ist bereits verlinkt!");
                                        getTs3Api().sendPrivateMessage(clientID, "Schreibe auf dem Minecraft Server '/ts unlink' um die alte Verlinkung zu entfernen");
                                    } else {
                                        getTs3Api().sendPrivateMessage(clientID, "Du bist bereits verlinkt!");
                                    }
                                    return;
                                }
                            }
                            String uniqueID = clientInfo.getUniqueIdentifier();
                            getTs3Api().sendPrivateMessage(clientID, "Du wurdest erfolgreich mit dem Minecraft Account " + nameResultByName.getName() + " verlinkt!");
                            teamSpeakProvider.verifyPlayer(nameResultByName.getUuid(), uniqueID, true);
                            HashMap<ClientProperty, String> options = new HashMap<>();
                            options.put(ClientProperty.CLIENT_DESCRIPTION, "Spielername: " + nameResultByName.getName());
                            getTs3Api().editClient(clientID, options);
                        } else {
                            getTs3Api().sendPrivateMessage(clientID, "Benutzung: !link <Ingame-Name>");
                        }
                    } else if (message.toLowerCase().equals("!link")) {
                        getTs3Api().sendPrivateMessage(clientID, "Benutzung: !link <Ingame-Name>");
                    }
                })).start();
            }

            public void onServerEdit(ServerEditedEvent event) {
            }

            public void onClientMoved(ClientMovedEvent event) {
                if (getTs3Api().getClientInfo(event.getClientId()).getChannelId() == 553882) {
                    ClientInfo player = getTs3Api().getClientInfo(event.getClientId());
                    for (Client staff : getTs3Api().getClients()) {
                        if (staff.isInServerGroup(1107))
                            getTs3Api().sendPrivateMessage(staff.getId(), "Der Spieler [URL=client://" + player
                                    .getId() + "/" + player.getUniqueIdentifier() + "~" + player
                                    .getPhoneticNickname() + "]" + player.getNickname() + "[/URL] hat den Support betreten.");
                    }
                }
            }

            public void onClientLeave(ClientLeaveEvent event) {

            }

            public void onClientJoin(ClientJoinEvent event) {
                boolean verified = teamSpeakProvider.getAllVerifications().contains(event.getUniqueClientIdentifier());
                (new Thread(() -> {
                    String id = event.getUniqueClientIdentifier();
                    int clientID = event.getClientId();
                    if (!verified) {
                        ClientInfo clientInfo = getTs3Api().getClientInfo(clientID);
                        if (clientInfo == null)
                            return;
                        teamSpeakProvider.removeAllGroups(getTs3Api(), clientInfo);
                        getTs3Api().sendPrivateMessage(clientID, "Du bist nicht mit deinem Minecraft-Account verlinkt!");
                        getTs3Api().sendPrivateMessage(clientID, "Schreibe in dieses Chat-Fenster !link <MinecraftName>, um dies zu tun!");
                        getTs3Api().sendPrivateMessage(clientID, "Danach erhdu deinen Rang und die Server Gruppe 'Verifiziert', wodurch du den Support betreten kannst.");
                        HashMap<ClientProperty, String> options = new HashMap<>();
                        options.put(ClientProperty.CLIENT_DESCRIPTION, "UID: " + clientInfo.getUniqueIdentifier());
                        getTs3Api().editClient(clientID, options);
                    } else {
                        UUID uuid = teamSpeakProvider.getUUIDByTSUID(event.getUniqueClientIdentifier());
                        ClientInfo clientInfo = getTs3Api().getClientInfo(clientID);
                        teamSpeakProvider.checkForGroups(uuid, clientID, getTs3Api(), clientInfo);
                        NameResult nameResultByUUID = nameStorageProvider.getNameResultByUUID(uuid);
                        HashMap<ClientProperty, String> options = new HashMap<>();
                        options.put(ClientProperty.CLIENT_DESCRIPTION, "Spielername: " + nameResultByUUID.getName());
                        getTs3Api().editClient(clientID, options);
                    }
                })).start();
            }

            public void onChannelEdit(ChannelEditedEvent e) {
            }

            public void onChannelDescriptionChanged(ChannelDescriptionEditedEvent e) {
            }

            public void onChannelCreate(ChannelCreateEvent e) {
            }

            public void onChannelDeleted(ChannelDeletedEvent e) {
            }

            public void onChannelMoved(ChannelMovedEvent e) {
            }

            public void onChannelPasswordChanged(ChannelPasswordChangedEvent e) {
            }

            public void onPrivilegeKeyUsed(PrivilegeKeyUsedEvent e) {
            }
        });
    }
}
