package net.ravenix.teamspeak.listener;

import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.PostLoginEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import net.ravenix.teamspeak.TeamSpeakPlugin;
import net.ravenix.teamspeak.ts.TeamSpeakProvider;

public final class PostLoginListener
        implements Listener {

    @EventHandler
    public void postLogin(PostLoginEvent event) {
        ProxiedPlayer player = event.getPlayer();

        TeamSpeakProvider teamSpeakProvider = TeamSpeakPlugin.getInstance().getTeamSpeakProvider();
        teamSpeakProvider.createPlayer(player.getUniqueId(), true);
    }

}
