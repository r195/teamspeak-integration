package net.ravenix.teamspeak.listener;

import de.dytanic.cloudnet.CloudNet;
import de.dytanic.cloudnet.common.document.gson.JsonDocument;
import de.dytanic.cloudnet.driver.CloudNetDriver;
import de.dytanic.cloudnet.driver.event.EventListener;
import de.dytanic.cloudnet.driver.event.events.channel.ChannelMessageReceiveEvent;
import net.md_5.bungee.api.ProxyServer;
import net.ravenix.core.bungee.BungeeCore;
import net.ravenix.core.shared.name.INameStorageProvider;
import net.ravenix.core.shared.name.NameResult;
import net.ravenix.core.shared.permission.IPermissionProvider;
import net.ravenix.core.shared.permission.PrefixType;
import net.ravenix.core.shared.permission.user.IPermissionUser;
import net.ravenix.teamspeak.TeamSpeakPlugin;
import net.ravenix.teamspeak.bot.TeamSpeakBot;
import net.ravenix.teamspeak.ts.TeamSpeakProvider;

import java.util.UUID;

public final class PubSubListener {

    @EventListener
    public void messageRecieved(ChannelMessageReceiveEvent event) {
        if (event.getChannel().equalsIgnoreCase("restartTSBot")) {
            if (CloudNetDriver.getInstance().getComponentName().equalsIgnoreCase("Proxy-1")) {
                TeamSpeakBot teamSpeakBot = TeamSpeakPlugin.getInstance().getTeamSpeakBot();
                teamSpeakBot.restart();
            }
        }
        TeamSpeakProvider teamSpeakProvider = TeamSpeakPlugin.getInstance().getTeamSpeakProvider();
        if (event.getChannel().equalsIgnoreCase("permissionUpdate")) {
            JsonDocument jsonDocument = event.getData();
            UUID uuid = UUID.fromString(jsonDocument.getString("uuid"));
            String tsuid = teamSpeakProvider.getTSUID(uuid);
            if (tsuid != null) {
                if (!tsuid.equalsIgnoreCase("noid")) {
                    if (CloudNetDriver.getInstance().getComponentName().equalsIgnoreCase("Proxy-1")) {
                        TeamSpeakBot teamSpeakBot = TeamSpeakPlugin.getInstance().getTeamSpeakBot();
                        teamSpeakProvider.checkForGroups(uuid, teamSpeakBot.getTs3Api().getClientByUId(tsuid).getId(), teamSpeakBot.getTs3Api(), teamSpeakBot.getTs3Api().getClientByUId(tsuid));
                    }
                }
            }
        }
        if (event.getChannel().equalsIgnoreCase("editSupportChannel")) {
            JsonDocument jsonDocument = event.getData();
            UUID uuid = UUID.fromString(jsonDocument.getString("uuid"));
            boolean open = jsonDocument.getBoolean("open");

            IPermissionProvider permissionProvider = BungeeCore.getInstance().getPermissionProvider();
            INameStorageProvider nameStorageProvider = BungeeCore.getInstance().getNameStorageProvider();
            IPermissionUser permissionUser = permissionProvider.getPermissionUser(uuid);
            NameResult nameResultByUUID = nameStorageProvider.getNameResultByUUID(uuid);

            if (open) {
                ProxyServer.getInstance().getPlayers().forEach(proxiedPlayer -> {
                    if (proxiedPlayer.hasPermission("ravenix.team")) {
                        proxiedPlayer.sendMessage(BungeeCore.getInstance().getPrefix() + permissionUser.getHighestGroup().getPrefix(PrefixType.DISPLAY) + nameResultByUUID.getName() + " §7hat den Support geöffnet.");
                    }
                });
            } else {
                ProxyServer.getInstance().getPlayers().forEach(proxiedPlayer -> {
                    if (proxiedPlayer.hasPermission("ravenix.team")) {
                        proxiedPlayer.sendMessage(BungeeCore.getInstance().getPrefix() + permissionUser.getHighestGroup().getPrefix(PrefixType.DISPLAY) + nameResultByUUID.getName() + " §7hat den Support geschlossen.");
                    }
                });
            }

            teamSpeakProvider.editSupportChannel(uuid, open, false);
        }
        if (event.getChannel().equalsIgnoreCase("tsVerify")) {
            JsonDocument jsonDocument = event.getData();
            UUID uuid = UUID.fromString(jsonDocument.getString("uuid"));
            String tsUID = jsonDocument.getString("tsUID");

            teamSpeakProvider.verifyPlayer(uuid, tsUID, false);
        }
        if (event.getChannel().equalsIgnoreCase("tsunVerify")) {
            JsonDocument jsonDocument = event.getData();
            UUID uuid = UUID.fromString(jsonDocument.getString("uuid"));

            teamSpeakProvider.unverifyPlayer(uuid, false);
        }
        if (event.getChannel().equalsIgnoreCase("createTSPlayer")) {
            JsonDocument jsonDocument = event.getData();
            UUID uuid = UUID.fromString(jsonDocument.getString("uuid"));

            teamSpeakProvider.createPlayer(uuid, false);
        }
    }

}
