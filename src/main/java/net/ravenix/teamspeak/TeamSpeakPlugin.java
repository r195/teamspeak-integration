package net.ravenix.teamspeak;

import de.dytanic.cloudnet.driver.CloudNetDriver;
import lombok.Getter;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.api.plugin.PluginManager;
import net.ravenix.core.bungee.BungeeCore;
import net.ravenix.teamspeak.bot.TeamSpeakBot;
import net.ravenix.teamspeak.commands.TeamSpeakCommand;
import net.ravenix.teamspeak.listener.PostLoginListener;
import net.ravenix.teamspeak.listener.PubSubListener;
import net.ravenix.teamspeak.ts.TeamSpeakProvider;

public final class TeamSpeakPlugin extends Plugin {

    @Getter
    private static TeamSpeakPlugin instance;

    @Getter
    private TeamSpeakBot teamSpeakBot;

    @Getter
    private TeamSpeakProvider teamSpeakProvider;

    @Override
    public void onEnable() {
        instance = this;

        this.teamSpeakBot = new TeamSpeakBot();
        if (CloudNetDriver.getInstance().getComponentName().equalsIgnoreCase("Proxy-1")) {
            this.teamSpeakBot.registerBot();
        }
        this.teamSpeakProvider = new TeamSpeakProvider(BungeeCore.getInstance().getMySQL());

        init();
    }

    private void init() {
        CloudNetDriver.getInstance().getEventManager().registerListener(new PubSubListener());

        PluginManager pluginManager = ProxyServer.getInstance().getPluginManager();
        pluginManager.registerListener(this, new PostLoginListener());
        pluginManager.registerCommand(this, new TeamSpeakCommand("ts"));
        pluginManager.registerCommand(this, new TeamSpeakCommand("teamspeak"));
    }

    @Override
    public void onDisable() {
        if (CloudNetDriver.getInstance().getComponentName().equalsIgnoreCase("Proxy-1")) {
            this.teamSpeakBot.unregister();
        }
    }
}
