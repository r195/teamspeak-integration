package net.ravenix.teamspeak.ts;

import com.github.theholywaffle.teamspeak3.TS3Api;
import com.github.theholywaffle.teamspeak3.api.ChannelProperty;
import com.github.theholywaffle.teamspeak3.api.ClientProperty;
import com.github.theholywaffle.teamspeak3.api.wrapper.ClientInfo;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import de.dytanic.cloudnet.CloudNet;
import de.dytanic.cloudnet.common.document.gson.JsonDocument;
import de.dytanic.cloudnet.driver.CloudNetDriver;
import net.ravenix.core.bungee.BungeeCore;
import net.ravenix.core.bungee.mysql.MySQL;
import net.ravenix.core.shared.permission.IPermissionProvider;
import net.ravenix.core.shared.permission.group.IPermissionGroup;
import net.ravenix.core.shared.permission.user.IPermissionUser;
import net.ravenix.teamspeak.TeamSpeakPlugin;
import net.ravenix.teamspeak.bot.TeamSpeakBot;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public final class TeamSpeakProvider {

    private final MySQL mySQL;

    private final Map<UUID, String> teamSpeakVerifications = Maps.newHashMap();
    private final Map<String, UUID> teamSpeakVerificationsByUID = Maps.newHashMap();
    private final List<String> allVerifications = Lists.newArrayList();

    public TeamSpeakProvider(MySQL mySQL) {
        this.mySQL = mySQL;

        this.createTables();
        this.loadTeamSpeakVerifications();
    }

    public String getTSUID(UUID uuid) {
        return this.teamSpeakVerifications.get(uuid);
    }

    public UUID getUUIDByTSUID(String tsUID) {
        return this.teamSpeakVerificationsByUID.get(tsUID);
    }

    public List<String> getAllVerifications() {
        return this.allVerifications;
    }

    public void createPlayer(UUID uuid, boolean send) {
        String tsuid = getTSUID(uuid);
        if (tsuid == null) {
            this.teamSpeakVerifications.put(uuid, "noid");
            if (CloudNetDriver.getInstance().getComponentName().equalsIgnoreCase("Proxy-1")) {
                this.mySQL.update("INSERT INTO teamSpeakVerifications (uuid,tsUID) VALUES ('" + uuid.toString() + "','noid')");
            }
            if (send) {
                createPlayer(uuid);
            }
        }
    }

    private void createPlayer(UUID uuid) {
        JsonDocument jsonDocument = new JsonDocument();
        jsonDocument.append("uuid", uuid.toString());

        CloudNetDriver.getInstance().getMessenger().sendChannelMessage("createTSPlayer", "update", jsonDocument);
    }

    public void verifyPlayer(UUID uuid, String tsUID, boolean send) {

        this.teamSpeakVerifications.remove(uuid);
        this.teamSpeakVerificationsByUID.remove(tsUID);
        this.teamSpeakVerifications.put(uuid, tsUID);
        this.teamSpeakVerificationsByUID.put(tsUID, uuid);
        this.allVerifications.add(tsUID);

        if (CloudNetDriver.getInstance().getComponentName().equalsIgnoreCase("Proxy-1")) {
            TeamSpeakBot teamSpeakBot = TeamSpeakPlugin.getInstance().getTeamSpeakBot();
            checkForGroups(uuid, teamSpeakBot.getTs3Api().getClientByUId(tsUID).getId(), teamSpeakBot.getTs3Api(), teamSpeakBot.getTs3Api().getClientByUId(tsUID));
            this.mySQL.update("UPDATE teamSpeakVerifications SET tsUID='" + tsUID + "' WHERE uuid='" + uuid.toString() + "'");
        }

        if (send) {

            verifyPlayer(uuid, tsUID);
        }
    }

    public void unverifyPlayer(UUID uuid, boolean send) {
        String tsuid = getTSUID(uuid);

        if (CloudNetDriver.getInstance().getComponentName().equalsIgnoreCase("Proxy-1")) {
            TeamSpeakBot teamSpeakBot = TeamSpeakPlugin.getInstance().getTeamSpeakBot();
            removeAllGroups(teamSpeakBot.getTs3Api(), teamSpeakBot.getTs3Api().getClientByUId(tsuid));

            ClientInfo clientInfo = teamSpeakBot.getTs3Api().getClientByUId(tsuid);
            HashMap<ClientProperty, String> options = new HashMap<>();
            options.put(ClientProperty.CLIENT_DESCRIPTION, "UID: " + clientInfo.getUniqueIdentifier());
            teamSpeakBot.getTs3Api().editClient(clientInfo.getId(), options);

        }

        this.allVerifications.remove(tsuid);
        this.teamSpeakVerifications.remove(uuid);
        this.teamSpeakVerificationsByUID.remove(tsuid);

        if (CloudNetDriver.getInstance().getComponentName().equalsIgnoreCase("Proxy-1")) {
            this.mySQL.update("UPDATE teamSpeakVerifications SET tsUID='noid' WHERE uuid='" + uuid.toString() + "'");
        }

        if (send) {
            unverifyPlayer(uuid);
        }
    }

    private void unverifyPlayer(UUID uuid) {
        JsonDocument jsonDocument = new JsonDocument();
        jsonDocument.append("uuid", uuid.toString());

        CloudNetDriver.getInstance().getMessenger().sendChannelMessage("tsunVerify", "update", jsonDocument);
    }

    private void verifyPlayer(UUID uuid, String tsUID) {
        JsonDocument jsonDocument = new JsonDocument();
        jsonDocument.append("uuid", uuid.toString());
        jsonDocument.append("tsUID", tsUID);

        CloudNetDriver.getInstance().getMessenger().sendChannelMessage("tsVerify", "update", jsonDocument);
    }

    public void editSupportChannel(UUID uuid, boolean open, boolean send) {
        int supportChannel = 553882;

        if (CloudNetDriver.getInstance().getComponentName().equalsIgnoreCase("Proxy-1")) {
            TeamSpeakBot teamSpeakBot = TeamSpeakPlugin.getInstance().getTeamSpeakBot();
            TS3Api ts3Api = teamSpeakBot.getTs3Api();
            if (open) {
                Map<ChannelProperty, String> openSETTINGS = new HashMap<>();
                openSETTINGS.put(ChannelProperty.CHANNEL_NAME, "┏ Support | Warteraum");
                openSETTINGS.put(ChannelProperty.CHANNEL_PASSWORD, "");
                openSETTINGS.put(ChannelProperty.CHANNEL_MAXCLIENTS, "5");
                ts3Api.editChannel(supportChannel, openSETTINGS);
            } else {
                Map<ChannelProperty, String> settings = new HashMap<>();
                settings.put(ChannelProperty.CHANNEL_NAME, "┏ Support | Warteraum [Geschlossen]");
                settings.put(ChannelProperty.CHANNEL_PASSWORD, "asdasdfdgsfdgdf");
                settings.put(ChannelProperty.CHANNEL_MAXCLIENTS, "0");
                ts3Api.editChannel(supportChannel, settings);
            }
        }

        if (send) {
            editSupportChannel(uuid, open);
        }
    }

    private void editSupportChannel(UUID uuid, boolean open) {
        JsonDocument jsonDocument = new JsonDocument();
        jsonDocument.append("uuid", uuid.toString());
        jsonDocument.append("open", open);

        CloudNetDriver.getInstance().getMessenger().sendChannelMessage("editSupportChannel", "update", jsonDocument);
    }

    public void checkForGroups(UUID uuid, int clientID, TS3Api ts3Api, ClientInfo clientInfo) {
        if (ts3Api == null) return;
        IPermissionProvider permissionProvider = BungeeCore.getInstance().getPermissionProvider();
        IPermissionUser permissionUser = permissionProvider.getPermissionUser(uuid);
        List<Integer> toAdd = Lists.newArrayList();
        List<Integer> toRemove = Lists.newArrayList();
        List<Integer> userGroups = Lists.newArrayList();
        if (clientInfo == null)
            clientInfo = ts3Api.getClientInfo(clientID);
        for (int groupID : clientInfo.getServerGroups())
            userGroups.add(groupID);
        int verygroup = 951;
        int rankGroup = getGroupIDByUUID(uuid);
        if (!userGroups.contains(verygroup))
            toAdd.add(verygroup);
        if (rankGroup != -1) {
            if (!userGroups.contains(rankGroup))
                toAdd.add(rankGroup);
        }
        for (IPermissionGroup permissionGroup : permissionProvider.getPermissionGroups()) {
            if (!permissionUser.getHighestGroup().equals(permissionGroup)) {
                Integer groupIDByGroup = getGroupIDByGroup(permissionGroup);
                if (clientInfo.isInServerGroup(groupIDByGroup))
                    toRemove.add(groupIDByGroup);
            }
        }
        for (Integer group : toAdd)
            ts3Api.addClientToServerGroup(group, clientInfo.getDatabaseId());
        for (Integer group : toRemove)
            ts3Api.removeClientFromServerGroup(group, clientInfo.getDatabaseId());
    }

    public void removeAllGroups(TS3Api ts3Api, ClientInfo clientInfo) {
        if (ts3Api == null) return;
        List<Integer> teamGroups = Lists.newArrayList();
        teamGroups.addAll(allGroupsToRemove());

        teamGroups.add(951);
        teamGroups.add(1107);
        if (clientInfo == null)
            return;
        for (int groupID : clientInfo.getServerGroups()) {
            if (teamGroups.contains(groupID))
                ts3Api.removeClientFromServerGroup(groupID, clientInfo.getDatabaseId());
        }
    }

    private List<Integer> allGroupsToRemove() {
        return Lists.newArrayList(619, 936, 939, 3155, 956, 957, 938, 2152, 959, 960, 950, 3235, 3234, 3237, 3236);
    }

    private Integer getGroupIDByGroup(IPermissionGroup permissionGroup) {
        if (permissionGroup.getName().equalsIgnoreCase("Administrator")) {
            return 619;
        }
        if (permissionGroup.getName().equalsIgnoreCase("Creator+")) {
            return 3235;
        }
        if (permissionGroup.getName().equalsIgnoreCase("Senior-Developer")) {
            return 936;
        }
        if (permissionGroup.getName().equalsIgnoreCase("Senior-Moderator")) {
            return 939;
        }
        if (permissionGroup.getName().equalsIgnoreCase("Senior-Content")) {
            return 3155;
        }
        if (permissionGroup.getName().equalsIgnoreCase("Developer")) {
            return 956;
        }
        if (permissionGroup.getName().equalsIgnoreCase("Moderator")) {
            return 957;
        }
        if (permissionGroup.getName().equalsIgnoreCase("Content")) {
            return 938;
        }
        if (permissionGroup.getName().equalsIgnoreCase("Junior-Moderator")) {
            return 2152;
        }
        if (permissionGroup.getName().equalsIgnoreCase("Builder")) {
            return 959;
        }
        if (permissionGroup.getName().equalsIgnoreCase("Designer")) {
            return 3234;
        }
        if (permissionGroup.getName().equalsIgnoreCase("Junior-Developer")) {
            return 3237;
        }
        if (permissionGroup.getName().equalsIgnoreCase("Junior-Content")) {
            return 3236;
        }
        if (permissionGroup.getName().equalsIgnoreCase("Creator")) {
            return 960;
        }
        if (permissionGroup.getName().equalsIgnoreCase("Premium")) {
            return 950;
        }
        return -1;
    }

    private Integer getGroupIDByUUID(UUID uuid) {
        IPermissionProvider permissionProvider = BungeeCore.getInstance().getPermissionProvider();
        IPermissionUser permissionUser = permissionProvider.getPermissionUser(uuid);
        IPermissionGroup permissionGroup = permissionUser.getHighestGroup();
        if (permissionGroup.getName().equalsIgnoreCase("Administrator")) {
            return 619;
        }
        if (permissionGroup.getName().equalsIgnoreCase("Creator+")) {
            return 3235;
        }
        if (permissionGroup.getName().equalsIgnoreCase("Senior-Developer")) {
            return 936;
        }
        if (permissionGroup.getName().equalsIgnoreCase("Senior-Moderator")) {
            return 939;
        }
        if (permissionGroup.getName().equalsIgnoreCase("Senior-Content")) {
            return 3155;
        }
        if (permissionGroup.getName().equalsIgnoreCase("Developer")) {
            return 956;
        }
        if (permissionGroup.getName().equalsIgnoreCase("Moderator")) {
            return 957;
        }
        if (permissionGroup.getName().equalsIgnoreCase("Content")) {
            return 938;
        }
        if (permissionGroup.getName().equalsIgnoreCase("Junior-Moderator")) {
            return 2152;
        }
        if (permissionGroup.getName().equalsIgnoreCase("Builder")) {
            return 959;
        }
        if (permissionGroup.getName().equalsIgnoreCase("Designer")) {
            return 3234;
        }
        if (permissionGroup.getName().equalsIgnoreCase("Junior-Developer")) {
            return 3237;
        }
        if (permissionGroup.getName().equalsIgnoreCase("Junior-Content")) {
            return 3236;
        }
        if (permissionGroup.getName().equalsIgnoreCase("Creator")) {
            return 960;
        }
        if (permissionGroup.getName().equalsIgnoreCase("Premium")) {
            return 950;
        }
        return -1;
    }

    private void createTables() {
        this.mySQL.update("CREATE TABLE IF NOT EXISTS teamSpeakVerifications (uuid varchar(36), tsUID varchar(100))");
    }

    private void loadTeamSpeakVerifications() {
        ResultSet resultSet = this.mySQL.getResult("SELECT * FROM teamSpeakVerifications");
        while (true) {
            try {
                if (!resultSet.next()) break;

                UUID uuid = UUID.fromString(resultSet.getString("uuid"));
                String tsUID = resultSet.getString("tsUID");

                this.teamSpeakVerifications.put(uuid, tsUID);
                if (!tsUID.equalsIgnoreCase("noid")) {
                    this.teamSpeakVerificationsByUID.put(tsUID, uuid);
                    this.allVerifications.add(tsUID);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
