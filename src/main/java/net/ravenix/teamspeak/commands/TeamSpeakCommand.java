package net.ravenix.teamspeak.commands;

import de.dytanic.cloudnet.common.document.gson.JsonDocument;
import de.dytanic.cloudnet.driver.CloudNetDriver;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.ravenix.core.bungee.BungeeCore;
import net.ravenix.teamspeak.TeamSpeakPlugin;
import net.ravenix.teamspeak.bot.TeamSpeakBot;
import net.ravenix.teamspeak.ts.TeamSpeakProvider;

public final class TeamSpeakCommand extends Command {

    public TeamSpeakCommand(String name) {
        super(name);
    }

    @Override
    public void execute(CommandSender commandSender, String[] args) {
        ProxiedPlayer proxiedPlayer = (ProxiedPlayer)commandSender;
        TeamSpeakProvider teamSpeakProvider = TeamSpeakPlugin.getInstance().getTeamSpeakProvider();
        if (args.length == 0) {
            proxiedPlayer.sendMessage(BungeeCore.getInstance().getPrefix() + "§b/ts unlink");
            if (proxiedPlayer.hasPermission("teamSpeak.support")) {
                proxiedPlayer.sendMessage(BungeeCore.getInstance().getPrefix() + "§b/ts support <open/close>");
            }
            if (proxiedPlayer.hasPermission("teamSpeak.restart")) {
                proxiedPlayer.sendMessage(BungeeCore.getInstance().getPrefix() + "§b/ts restart");
            }
            return;
        }
        if (args.length == 1) {
            if (args[0].equalsIgnoreCase("unlink")) {
                String tsuid = teamSpeakProvider.getTSUID(proxiedPlayer.getUniqueId());
                if (tsuid == null) {
                    proxiedPlayer.sendMessage(BungeeCore.getInstance().getPrefix() + "§cDu bist nicht verlinkt.");
                    return;
                }
                if (tsuid.equalsIgnoreCase("noid")) {
                    proxiedPlayer.sendMessage(BungeeCore.getInstance().getPrefix() + "§cDu bist nicht verlinkt.");
                    return;
                }
                teamSpeakProvider.unverifyPlayer(proxiedPlayer.getUniqueId(), true);
                proxiedPlayer.sendMessage(BungeeCore.getInstance().getPrefix() + "§aDu hast die Verknüpfung erfolgreich aufgehoben.");
                return;
            }
            if (args[0].equalsIgnoreCase("restart")) {
                if (proxiedPlayer.hasPermission("teamSpeak.restart")) {
                    TeamSpeakBot teamSpeakBot = TeamSpeakPlugin.getInstance().getTeamSpeakBot();
                    if (CloudNetDriver.getInstance().getComponentName().equalsIgnoreCase("Proxy-1")) {
                        teamSpeakBot.restart();
                    } else {
                        restartBot();
                    }
                    proxiedPlayer.sendMessage(BungeeCore.getInstance().getPrefix() + "§7TeamSpeak Bot wird neugestartet");
                    return;
                }
            }
        }
        if (args.length == 2) {
            if (args[0].equalsIgnoreCase("support")) {
                if (proxiedPlayer.hasPermission("teamSpeak.support")) {
                    if (args[1].equalsIgnoreCase("close")) {
                        teamSpeakProvider.editSupportChannel(proxiedPlayer.getUniqueId(), false, true);
                        proxiedPlayer.sendMessage(BungeeCore.getInstance().getPrefix() + "§7Du hast den Support geschlossen.");
                        return;
                    }
                    if (args[1].equalsIgnoreCase("open")) {
                        teamSpeakProvider.editSupportChannel(proxiedPlayer.getUniqueId(), true, true);
                        proxiedPlayer.sendMessage(BungeeCore.getInstance().getPrefix() + "§7Du hast den Support geöffnet.");
                        return;
                    }
                }
            }
        }
        proxiedPlayer.sendMessage(BungeeCore.getInstance().getPrefix() + "§b/ts unlink");
        if (proxiedPlayer.hasPermission("teamSpeak.restart")) {
            proxiedPlayer.sendMessage(BungeeCore.getInstance().getPrefix() + "§b/ts restart");
        }
    }
    private void restartBot() {
        JsonDocument jsonDocument = new JsonDocument();

        CloudNetDriver.getInstance().getMessenger().sendChannelMessage("restartTSBot", "update", jsonDocument);
    }
}
